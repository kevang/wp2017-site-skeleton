wp2017-site-skeleton
====================

Site skeleton for the course
[Web Programming](http://www.rug.nl/ocasys/rug/vak/show?code=LIX018P05). Make a
fresh clone of this repository for each assignment and base your work on this
structure.
