<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="main.css">
    <title>My first application</title>
  </head>
  <body>
    <h1>My first application</h1>
    <p>Welcome to <i>Web Programming</i>!</p>
    <script src="main.js"></script>
  </body>
</html>
